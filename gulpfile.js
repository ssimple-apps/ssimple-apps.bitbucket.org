'use strict';

var gulp = require('gulp');
var connect = require('gulp-connect');
var cache = require('gulp-cache');
var watch = require('gulp-watch');
var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var minifyCss = require('gulp-minify-css');
var jsonminify = require('gulp-jsonminify');
var mainBowerFiles = require('main-bower-files');
var gulpif = require('gulp-if');
var sourcemaps = require("gulp-sourcemaps");
var source = require('vinyl-source-stream');
var streamify = require('gulp-streamify');
var gutil = require('gulp-util');
var compressing = true;

gulp.task('clean', function (cb) {
    cache.clearAll();
    require('rimraf')('app/dist', cb);
});

gulp.task('bower-files', function(){
  return gulp.src(mainBowerFiles({
            overrides: {
                bootstrap: {
                    main: [
                        './dist/js/bootstrap.js',
                        './dist/css/*.min.*',
                        './dist/fonts/*.*'
                    ]
                }
            }
        }))
    .pipe(gulpif(compressing, gulpif('*.js', uglify())))
    .pipe(gulpif(compressing, gulpif('*.css', minifyCss())))
    .pipe(gulp.dest("app/dist/libs"));
});

gulp.task('scripts', function() {
	 return gulp.src('./app/scripts/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(gulpif(compressing, streamify(uglify())))
    .pipe(gulp.dest('app/dist/scripts'))
    .pipe(connect.reload());
});

gulp.task('distserver', function () {
  console.log("starting dist server");
  connect.server({
    root: 'app',
    port: 9001,
    livereload: true
  });
});

gulp.task('watch', function() {
  gulp.watch('./app/index.html', ['scripts']);
  gulp.watch('./app/styles/**/*.css', ['scripts']);
  gulp.watch('./app/inits/**/*.js', ['scripts']);
  gulp.watch('./app/lookup/**/*.json', ['scripts']);
  gulp.watch('./app/scripts/**/*.js', ['scripts']);
  gulp.watch('./app/img/**/*', ['scripts']);
});

gulp.task('build', ['bower-files', 'scripts']);

gulp.task('serve', ['distserver', 'watch']);

gulp.task('dev', ['clean'], function () {
  gulp.start('build');
});

gulp.task('default', ['dev']);
